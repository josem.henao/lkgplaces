package co.com.linkage.places.apirest.model;

import lombok.Data;

@Data
public class Place {
    private Long id;
    private String name;
    private Long idCity;
}
