package co.com.linkage.places.apirest.converter;

import co.com.linkage.places.apirest.entity.PlaceEntity;
import co.com.linkage.places.apirest.model.Place;
import co.com.linkage.places.apirest.validator.PlaceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceConverter {
    @Autowired
    private PlaceValidator placeValidator;

    public Place entityToModel(PlaceEntity placeEntity) {
        Place place= new Place();
        place.setId(placeEntity.getId());
        place.setName(placeEntity.getName());
        place.setIdCity(placeEntity.getIdCity());
        return place;
    }

    public PlaceEntity modelToEntity(Place place) throws BindException {
        PlaceEntity placeEntity = new PlaceEntity();
        placeEntity.setId(place.getId());
        placeEntity.setName(place.getName());
        placeEntity.setIdCity(place.getIdCity());
        placeValidator.validate(placeEntity);
        return placeEntity;
    }


    public List<Place> entityToModel(List<PlaceEntity> placesEntity) {
        List<Place> places = new ArrayList<>(placesEntity.size());

        placesEntity.forEach((entity)->{
            places.add(entityToModel(entity));
        });

        return places;
    }
}
