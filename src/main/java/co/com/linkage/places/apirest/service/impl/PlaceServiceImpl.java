package co.com.linkage.places.apirest.service.impl;
import co.com.linkage.places.apirest.converter.PlaceConverter;
import co.com.linkage.places.apirest.entity.PlaceEntity;
import co.com.linkage.places.apirest.model.Place;
import co.com.linkage.places.apirest.repository.PlaceRepository;
import co.com.linkage.places.apirest.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import java.util.List;

@Service
public class PlaceServiceImpl implements PlaceService {

    @Autowired
    private PlaceRepository placeRepository;

    @Autowired
    private PlaceConverter placeConverter;

    @Override
    public List<Place> listAll(){
        return placeConverter.entityToModel(placeRepository.findAll());
    }

    @Override
    public Place findById(Long id) {
        return placeConverter.entityToModel(placeRepository.findById(id).orElse(null));
    }

    @Override
    public void save(Place place) throws BindException {
        placeRepository.save(placeConverter.modelToEntity(place));
    }

    @Override
    public PlaceEntity update(Place place, Long id) {
        return placeRepository.findById(id)
                .map(placeEntity -> {
                    placeEntity.setName(place.getName());
                    placeEntity.setIdCity(place.getIdCity());
                    return placeRepository.save(placeEntity);
                })
                .orElseGet(() -> {
                    place.setId(id);
                    PlaceEntity placeEntity = null;
                    try {
                        placeEntity = placeConverter.modelToEntity(place);
                    } catch (BindException e) {
                        e.printStackTrace();
                    }
                    return placeRepository.save(placeEntity);
                });
    }

    @Override
    public void delete(Long id){
        placeRepository.deleteById(id);
    }


}
