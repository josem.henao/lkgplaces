package co.com.linkage.places.apirest.validator.generic;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

@Component
public class GenericValidator<T> {
    public GenericValidator(){
        super();
    }

    public void validate(T entity) throws BindException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        for (ConstraintViolation<T> violation :validator.validate(entity)){
            throw new BindException("Validation Error", violation.getMessage());
        }
    }
}
