package co.com.linkage.places.apirest.restcontroller;

import co.com.linkage.places.apirest.model.Place;
import co.com.linkage.places.apirest.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/places")
public class PlaceController {
    @Autowired
    private PlaceService placeService;

    @GetMapping("/")
    List<Place> all() {
        return placeService.listAll();
    }

    @GetMapping("/{id}")
    Place one(@PathVariable Long id) {
        return placeService.findById(id);
    }

    @PostMapping("/")
    void newPlace(@RequestBody Place newPlace) throws BindException {
        placeService.save(newPlace);
    }

    @PutMapping("/{id}")
    void replacePlace(@RequestBody Place newPlace, @PathVariable Long id) throws BindException {
        placeService.update(newPlace, id);
    }

    @DeleteMapping("/{id}")
    void deleteEmployee(@PathVariable Long id) {
        placeService.delete(id);
    }


}
