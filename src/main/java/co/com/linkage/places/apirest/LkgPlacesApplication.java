package co.com.linkage.places.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LkgPlacesApplication {

    public static void main(String[] args) {
        SpringApplication.run(LkgPlacesApplication.class, args);
    }

}
