package co.com.linkage.places.apirest.service;

import co.com.linkage.places.apirest.entity.PlaceEntity;
import co.com.linkage.places.apirest.model.Place;
import org.springframework.validation.BindException;

import java.util.List;

public interface PlaceService {
    List<Place> listAll();

    Place findById(Long id);

    void save(Place place) throws BindException;

    PlaceEntity update(Place place, Long id) throws BindException;

    void delete(Long id);
}
