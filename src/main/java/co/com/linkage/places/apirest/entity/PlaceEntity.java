package co.com.linkage.places.apirest.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;

@Entity
@Table(name = "places")
@Data
public class PlaceEntity {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    //@Size(min = 3, max = 50, message = "name lenght invalid")
    @NotNull(message = "name mustn't be void")
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    //@Size(min = 3, max = 32, message = "idCity invalid")
    @NotNull(message = "idCity mustn't be void")
    @Column(name = "id_city", nullable = false, length = 50)
    private Long idCity;
}