package co.com.linkage.places.apirest.repository;

import co.com.linkage.places.apirest.entity.PlaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<PlaceEntity, Long> {

}
