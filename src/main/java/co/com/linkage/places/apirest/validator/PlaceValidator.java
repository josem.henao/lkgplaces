package co.com.linkage.places.apirest.validator;

import co.com.linkage.places.apirest.entity.PlaceEntity;
import co.com.linkage.places.apirest.validator.generic.GenericValidator;
import org.springframework.stereotype.Component;

@Component
public class PlaceValidator extends GenericValidator<PlaceEntity> {
    public PlaceValidator(){
        super();
    }
}
